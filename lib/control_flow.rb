# EASY

# Return the argument with all its lowercase characters removed.
def destructive_uppercase(str)
  lowercases = "abcdefghijklmnopqrstuvwxyz"
  str.delete(lowercases)
end

# Return the middle character of a string. Return the middle two characters if
# the word is of even length, e.g. middle_substring("middle") => "dd",
# middle_substring("mid") => "i"
def middle_substring(str)
  if str.length.even?
    return str[(str.length/2)-1..str.length/2]
  else return str[(str.length/2)]
  end
end

# Return the number of vowels in a string.
VOWELS = %w(a e i o u)
def num_vowels(str)
  str.downcase.count("aeiou")
end

# Return the factoral of the argument (num). A number's factorial is the product
# of all whole numbers between 1 and the number itself. Assume the argument will
# be > 0.
def factorial(num)
  numbers = (1..num).to_a
  factorial=1
  index = 0
  while index < numbers.length
    factorial = factorial*numbers[index]
    index+=1
  end
  factorial
end


# MEDIUM

# Write your own version of the join method. separator = "" ensures that the
# default seperator is an empty string.
def my_join(arr, separator = "")
  count = 0
  return "" if arr == []
  new_string=""
  while count < arr.length-1
    new_string << arr[count] << separator
    count+=1
  end
  new_string << arr[-1]
  new_string
end

# Write a method that converts its argument to weirdcase, where every odd
# character is lowercase and every even is uppercase, e.g.
# weirdcase("weirdcase") => "wEiRdCaSe"
def weirdcase(str)
  characters = str.chars
  new_str = []
  count = 0
  while count < str.length
    if count.even?
      new_str.push(characters[count].downcase)
    else new_str.push(characters[count].upcase)
    end
    count+=1
  end
  new_str.join
end

# Reverse all words of five more more letters in a string. Return the resulting
# string, e.g., reverse_five("Looks like my luck has reversed") => "skooL like
# my luck has desrever")
def reverse_five(str)
  words = str.split(" ")
  count = 0
  while count < words.length
    if words[count].length >= 5
      words[count]=words[count].reverse
    end
    count+=1
  end
  words.join(" ")
end

# Return an array of integers from 1 to n (inclusive), except for each multiple
# of 3 replace the integer with "fizz", for each multiple of 5 replace the
# integer with "buzz", and for each multiple of both 3 and 5, replace the
# integer with "fizzbuzz".
def fizzbuzz(n)
  fizzbuzz=(1..n).to_a
  fizzbuzz.each_with_index do |int,idx|
    if (int%5==0) && (int%3==0)
      fizzbuzz[idx]="fizzbuzz"
    elsif int%5==0
      fizzbuzz[idx]="buzz"
    elsif int%3==0
      fizzbuzz[idx]="fizz"
    end
  end
  fizzbuzz
end


# HARD

# Write a method that returns a new array containing all the elements of the
# original array in reverse order.
def my_reverse(arr)
  reverse_arr = []
  count = arr.length-1
  while count > -1
    reverse_arr.push(arr[count])
    count=count-1
  end
  reverse_arr
end

# Write a method that returns a boolean indicating whether the argument is
# prime.
def prime?(num)
  return false if num == 1 || num == 0
  numbers = (2...num).to_a
  counter = 0
  while counter < numbers.length
    if num%numbers[counter] == 0
      return false
    end
    counter+=1
  end
  return true
end

# Write a method that returns a sorted array of the factors of its argument.
def factors(num)
  numbers = (1..num).to_a
  factors = []
  count = 0
  while count < numbers.length
    if num%numbers[count]==0
      factors.push(numbers[count])
    end
    count+=1
  end
  factors
end

# Write a method that returns a sorted array of the prime factors of its argument.
def prime_factors(num)
  all_factors = factors(num)
  counter = 0
  prime_factors = []
  while counter < all_factors.length
    if prime?(all_factors[counter])
      prime_factors.push(all_factors[counter])
    end
    counter+=1
  end
  prime_factors
end

# Write a method that returns the number of prime factors of its argument.
def num_prime_factors(num)
  prime_factors(num).length
end


# EXPERT

# Return the one integer in an array that is even or odd while the rest are of
# opposite parity, e.g. oddball([1,2,3]) => 2, oddball([2,4,5,6] => 5)
def oddball(arr)
  odds = []
  evens = []
  counter = 0
  while counter < arr.length
    if arr[counter].odd?
      odds.push(arr[counter])
    else evens.push(arr[counter])
    end
    counter+=1
  end
  if odds.length == 1
    return odds[0]
  else return evens[0]
  end
end
